Using Ansible to backup and archive all the Gitlab repositories to ensure a recover point if required to do so. The gitlab host needs to be separate from  the control node for this to complete successfully.

 #### Components

- [gitlab_backups.sh](gitlab_backups.sh) - calls `gitlab_backups.yml` at `15 06 * * 1-5`
- [gitlab_backups.yml](gitlab_backups.yml)
   - Find archives older than 7 days
   - Remove archives older 7 days
   - Synchronize daily backup to control node
   - Find archives older than 10 days on control node
   - Remove archives older 10 days on control node

#### Installation
As the process is scheduled to run via crond no user interaction is required. Howver if you are using the non interactive version  calling `gitlab_backups.sh` then you would run `$ ansible-playbook --extra-vars '@passwd.yml' gitlab_backups.yml` however this assumes you have the ansible-vault password and it has been defined in the appropiate `ansible.cfg` otherwise you need to append [--ask-vault-pass](https://docs.ansible.com/ansible/latest/user_guide/vault.html) for this too complete sucessfully.

Alternatively you can run `$ ansible-playbook gitlab_backups.yml -b -K` and provide the appopropiate escalated password for it to complete successfully.


#### Summary
A simple automated process to easily backup/archive the Gitlab repositories on the target _host_ and then securely sync to the control node to ensure multiple copies are kept in different locations, should there be an outage in the future or a desire to roll back.
